package com.example.tictactoe.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.tictactoe.R;

import java.util.ArrayList;

public class JeuFragment extends Fragment {

    private JeuViewModel jeuViewModel;

    //creation de la matrice qui verifie si on gagne
    //        [vertical]  [horizontal]
    public int[][] mat;

    //creation du arraylist contenant les btn
    ArrayList<Button> lBtn;

    //creation de player1
    Boolean player1;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        jeuViewModel =
                ViewModelProviders.of(this).get(JeuViewModel.class);
        View root = inflater.inflate(R.layout.fragment_jeu, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        jeuViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        iniJeu(root);
        return root;

    }


    //methode dini du jeu pour creer les btn et listeners
    public void iniJeu(View root){
        //principe de 2 jour avec une valeyr bool
        //instanciation de player1
        player1=true;

        //instanciation de la mat pour voir le gagnant
         mat=new int [3][3];
        //declaration des btn******************************
        // je les mets dans un array
        lBtn = new ArrayList<Button>();
        //parcour la list pour les instancier et ajoute le onlick listenenr
        //boucle sur les elem du tableau(les btn)il y a 9btn donc le i doit se rendre jusqua 9 exclusivement
        //les declare un a un en ajoutant le onclicklistener et la methode du click qui ajoute un X dans la case
        for (int i=0; i<9;i++){
            String btnId = "button"+(i+1);
            int resId = getResources().getIdentifier(btnId, "id", getActivity().getPackageName());
            final Button btn = (Button) root.findViewById(resId);
            btn.setTextSize(30);
            lBtn.add(btn);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnClick(btn);

                }
            });
        }
    }
    //methode lorsquun bouton est clique
    public void btnClick(Button btn){

        //verifi si la partie est terminee


            if (btn.getText() == "") {
                //regarde cest quel joueur qui joue
                if (player1)
                    btn.setText("X");
                else
                    btn.setText("O");
                //ajoute le btn a la matrice
                ajoutMat(btn);



                //check sil y a un gagnant
                if (verifGagne()) {
                    //pop up window pour declarer le gagnant

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    //bouton pour recommencer
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //recommence la partie
                            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                            navController.navigate(R.id.nav_jeu);
                        }
                    });

                    //afficher qui est le gagnant selon le tour du joueur
                    String bravo;
                    if (player1)
                        bravo = getResources().getString(R.string.msg_bravo) + " 1";
                    else
                        bravo = getResources().getString(R.string.msg_bravo) + " 2";
                    builder.setMessage(bravo)
                            .setTitle(R.string.dialog_title);

                    //creer le alerdialog et le montre
                    AlertDialog dialog = builder.create();
                    dialog.show();



                }

                //regarde si la planche de jue est pleine
                else if(checkFull())
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    //bouton pour recommencer
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //recommence la partie
                            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                            navController.navigate(R.id.nav_jeu);
                        }
                    });

                    builder.setMessage(R.string.nul)
                            .setTitle(R.string.dialog_title);

                    //creer le alerdialog et le montre
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                //inverse la valeur du bool pour changer de joueur
                player1 = !player1;
            } else
                Toast.makeText(getActivity().getApplicationContext(), "Case deja occupee", Toast.LENGTH_SHORT).show();

    }
    //methode pour verifier si il y a un gagnant
    public boolean verifGagne(){

        //declaration des variable qui continnent les sommes
        int sommeRangee=0;
        int sommeColonne=0;
        int sommeDiagBG=0;
        int sommeDiaHG=0;


        //check rangees/colonnes
        for(int y=0;y<3;y++){
            sommeRangee=0;
            sommeColonne=0;
            for(int x=0;x<3;x++){
                //calcule la somme horizontale
                sommeRangee+=mat[x][y];

                //calcule la somme verticale
                sommeColonne+=mat[y][x];

            }

            //regarde si une somme est egal a 3/-3, si oui, retourne vrai
            if(sommeColonne==3||sommeColonne==-3||sommeRangee==3||sommeRangee==-3)
                return true;
        }

        //check diagonales
        sommeDiaHG=0;
        sommeDiagBG=0;
        for(int i =0;i<3;i++)
        {
            //si mm coordonées alors diagonale
            sommeDiaHG+= mat[i][i];

            sommeDiagBG+=mat[2-i][i];
        }

        if(sommeDiaHG==3 || sommeDiaHG==-3||sommeDiagBG==3 || sommeDiagBG==-3)
            return true;

        return false;

    }

    //methode pour ajouter le btn a la matrice
    public void ajoutMat(Button btn){
        //get lindex du boutou dans le array
        int indexBtn =lBtn.indexOf(btn);


        //si lindex est elem [0,3[ premiere rangee, sinon si x e[3,6[ 2eme rangee, sinon derniere rangee
        //avec une valeur pour la posVerticale
        //enleve des valeurs a indexBtn pour que les valeurs respectent le nb de colonnes(plus petit que 3)
        if(indexBtn<3)
        {
            //ajoute 1 si joueur1 sinon ajoute -1
            if (player1)
                mat[indexBtn][0] = 1;
            else
                mat[indexBtn][0] = -1;
        }
        else if(indexBtn>=3 && indexBtn<6)
        {
            //ajoute 1 si joueur1 sinon ajoute -1
            if (player1)
                mat[indexBtn-3][1] = 1;
            else
                mat[indexBtn-3][1] = -1;
        }
        else
        {
            //ajoute 1 si joueur1 sinon ajoute -1
            if (player1)
                mat[indexBtn-6][2] = 1;
            else
                mat[indexBtn-6][2] = -1;
        }
    }

    //methode pour voir si la partie est fini
    public boolean checkFull (){
        int caseOccupees=0;

        //parcour la matrice
        for(int i =0; i<mat.length;i++)
        {
            for(int y=0; y<mat.length;y++)
            {
                if(mat[i][y]==1 ||mat[i][y]==-1)
                    caseOccupees++;
            }
        }
        //si toutes les cases son occupee retourne vrai
        if(caseOccupees==9)
            return true;

        return false;
    }


}