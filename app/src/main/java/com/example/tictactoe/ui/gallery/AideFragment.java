package com.example.tictactoe.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tictactoe.R;

public class AideFragment extends Fragment {

    private AideViewModel aideViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        aideViewModel =
                ViewModelProviders.of(this).get(AideViewModel.class);
        View root = inflater.inflate(R.layout.fragment_aide, container, false);
        final TextView textView = root.findViewById(R.id.text_gallery);
        aideViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        WebView browser = (WebView) root.findViewById(R.id.webview);
        browser.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }
        });
        browser.loadUrl("https://www.wikihow.com/Play-Tic-Tac-Toe");
        return root;
    }

}